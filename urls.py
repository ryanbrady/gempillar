from django.conf.urls.defaults import patterns, include, url
from django.views.generic.base import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ryanbrady.views.home', name='home'),
    # url(r'^ryanbrady/', include('ryanbrady.foo.urls')),
    url(r'^$', TemplateView.as_view(template_name="design.html")),
    url(r'^weblog/', include('zinnia.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
